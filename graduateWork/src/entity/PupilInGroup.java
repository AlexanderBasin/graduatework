package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class PupilInGroup implements GetProperties {
	private int idGroup=0;
	private int idPupil=0;
	public PupilInGroup(int idGroup, int idPupil) {
		this.idGroup = idGroup;
		this.idPupil = idPupil;
	}
	public int getIdGroup() {
		return idGroup;
	}
	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}
	public int getIdPupil() {
		return idPupil;
	}
	public void setIdPupil(int idPupil) {
		this.idPupil = idPupil;
	}
	@Override
	public String toString() {
		return "PupilInGroup [idGroup=" + idGroup + ", idPupil=" + idPupil + "]";
	}
	public PupilInGroup(){}

	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (idGroup!=0) {
			getProperties.put("id_group", idGroup);
		}
		if (idPupil!=0) {
			getProperties.put("id_pupil", idPupil);
		}
		
		
		
		return getProperties;
	}

	@Override
	public String getNameTable() {
		return "groups_pupils";
	}

	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		PupilInGroup pupilInGroup = new PupilInGroup();
		pupilInGroup.setIdGroup(resSet.getInt("id_group"));
		pupilInGroup.setIdPupil(resSet.getInt("id_pupil"));
		return pupilInGroup;
	}

}
