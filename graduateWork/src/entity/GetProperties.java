package entity;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public interface  GetProperties  {
	public abstract HashMap<String,Object> getProperties();
	public abstract String getNameTable();
	public abstract GetProperties setProperties( ResultSet resSet ) throws SQLException;
	
	//public abstract  HashMap<String,Object> getAllProperties();
}
