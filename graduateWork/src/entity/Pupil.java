package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Pupil extends User {
	private int idPupil =0;
	private String role =null;
	public Pupil() {
		
	}
	
	public Pupil(int idTeacher, String login, String password) {
		this.idPupil = idTeacher;
		super.login = login;
		super.password = password;
	}
	public int getIdPupil() {
		return idPupil;
	}
	public void setIdTeacher(int idPupil) {
		this.idPupil = idPupil;
	}

	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (this.idPupil!=0) {
			getProperties.put("id_pupil", idPupil);
		}
		if (super.login!=null) {
			getProperties.put("login",login);
		}
		if (super.password!=null) {
			getProperties.put("password",password);
		}
		return getProperties;
	}

	@Override
	public String getNameTable() {
		return "pupils";
	}
	
	

	@Override
	public String toString() {
		return "Pupil [idPupil=" + idPupil + ", role=" + role + ", login=" + login + ", password=" + password
				+ ", valid=" + valid + "]";
	}

	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		Pupil pupil = new Pupil();
		pupil.setIdTeacher(resSet.getInt("id_pupil"));
		pupil.setLogin(resSet.getString("login"));
		pupil.setPassword(resSet.getString("password")); 
		
		return pupil;
	}

	@Override
	public void setNameRole(String role) {
		this.role=role;
		
	}

	@Override
	public String getNameRole() {
		return role;
	}

}
