package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class TestInGroup implements GetProperties {
	private int idGroup=0;
	private int idTest=0;
	public TestInGroup(int idGroup, int idTest) {
		this.idGroup = idGroup;
		this.idTest = idTest;
	}
	public int getIdGroup() {
		return idGroup;
	}
	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}
	public int getIdTest() {
		return idTest;
	}
	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}
	@Override
	public String toString() {
		return "TestInGroup [idGroup=" + idGroup + ", idTest=" + idTest + "]";
	}
	public TestInGroup(){}

	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (idGroup!=0) {
			getProperties.put("id_group", idGroup);
		}
		if (idTest!=0) {
			getProperties.put("id_test", idTest);
		}
		
		
		return getProperties;
	}

	@Override
	public String getNameTable() {
		return "groups_tests";
	}

	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		TestInGroup testInGroup = new TestInGroup();
		testInGroup.setIdGroup(resSet.getInt("id_group"));
		testInGroup.setIdTest(resSet.getInt("id_test"));
		return testInGroup;
	}

}
