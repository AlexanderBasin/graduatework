package entity;


import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Teacher  extends User  {
	private int idTeacher =0;
	private String role =null;
	public Teacher() {
		
	}
	
	public Teacher(int idTeacher, String login, String password) {
		this.idTeacher = idTeacher;
		super.login = login;
		super.password = password;
	}
	public int getIdTeacher() {
		return idTeacher;
	}
	public void setIdTeacher(int idTeacher) {
		this.idTeacher = idTeacher;
	}
	
	
	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (this.idTeacher!=0) {
			getProperties.put("id_teacher", idTeacher);
		}
		if (super.login!=null) {
			getProperties.put("login",login);
		}
		if (super.password!=null) {
			getProperties.put("password",password);
		}
		return getProperties;
	}
	@Override
	public String getNameTable() {
		
		return "teachers";
	}
	
	

	@Override
	public String toString() {
		return "Teacher [idTeacher=" + idTeacher + ", role=" + role + ", login=" + login + ", password=" + password
				+ ", valid=" + valid + "]";
	}

	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		Teacher teacher = new Teacher();
		teacher.setIdTeacher(resSet.getInt("id_teacher"));
		teacher.setLogin(resSet.getString("login"));
		teacher.setPassword(resSet.getString("password")); 
		
		return teacher;
	}

	

	@Override
	public void setNameRole(String role) {
		this.role =role;
		
	}

	@Override
	public String getNameRole() {
		// TODO Auto-generated method stub
		return role;
	}



	

	

	

	
	
	
}
