package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Variant implements  GetProperties {
	private int idVariant = 0;
	private int idQuestion=0;
	private String textVariant =null;
	private boolean answer = false;
	public Variant(int idVariant, int idQuestion, String textVariant, boolean answer) {
		this.idVariant = idVariant;
		this.idQuestion = idQuestion;
		this.textVariant = textVariant;
		this.answer = answer;
	}
	public Variant(){}
	public int getIdVariant() {
		return idVariant;
	}
	public void setIdVariant(int idVariant) {
		this.idVariant = idVariant;
	}
	public int getIdQuestion() {
		return idQuestion;
	}
	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}
	public String getTextVariant() {
		return textVariant;
	}
	public void setTextVariant(String textVariant) {
		this.textVariant = textVariant;
	}
	public boolean isAnswer() {
		return answer;
	}
	public void setAnswer(boolean answer) {
		this.answer = answer;
	}
	@Override
	public String toString() {
		return "Variant [idVariant=" + idVariant + ", idQuestion=" + idQuestion + ", textVariant=" + textVariant
				+ ", answer=" + answer + "]";
	}

	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (idVariant!=0) {
			getProperties.put("id_variant", idVariant);
		}
		if (idQuestion!=0) {
			getProperties.put("id_question", idQuestion);
		}
		if (textVariant!=null) {
			getProperties.put("text_variant", textVariant);
		}
		if (answer!=false) {
			getProperties.put("answer", answer);
		}
		return getProperties;
	}

	@Override
	public String getNameTable() {
		// TODO Auto-generated method stub
		return "variants";
	}
	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		Variant variant = new Variant();
		variant.setIdVariant(resSet.getInt("id_variant"));
		variant.setIdQuestion(resSet.getInt("id_question"));
		variant.setTextVariant(resSet.getString("text_variant"));
		variant.setAnswer(resSet.getBoolean("answer"));
		return variant;
	}

}
