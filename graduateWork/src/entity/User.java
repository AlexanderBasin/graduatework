package entity;

public abstract class User implements GetProperties {
	protected String login =null;
	protected String password = null;
	protected boolean valid = false;
	
	public abstract void setNameRole (String role);
	public abstract String getNameRole();
	public void setValid (boolean newValid){
		this.valid = newValid;
	}
	public boolean isValid(){
		return valid;
	}
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}
