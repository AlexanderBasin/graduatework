package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Result implements GetProperties {
	private int idTeacher = 0;
	private int idTest = 0;
	private int idPupil = 0;
	private float progress = -1;
	
	public Result(int idTeacher, int idTest, int idPupil, float progress) {
		this.idTeacher = idTeacher;
		this.idTest = idTest;
		this.idPupil = idPupil;
		this.progress = progress;
	}
	public Result(){}
	

	public int getIdTeacher() {
		return idTeacher;
	}
	public void setIdTeacher(int idTeacher) {
		this.idTeacher = idTeacher;
	}
	public int getIdTest() {
		return idTest;
	}
	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}
	public int getIdPupil() {
		return idPupil;
	}
	public void setIdPupil(int idPupil) {
		this.idPupil = idPupil;
	}
	public double getProgress() {
		return progress;
	}
	public void setProgress(float progress) {
		this.progress = progress;
	}
	
	@Override
	public String toString() {
		return "Result [idTeacher=" + idTeacher + ", idTest=" + idTest + ", idPupil=" + idPupil + ", progress="
				+ progress + "]";
	}
	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (idTeacher!=0) {
			getProperties.put("id_teacher", idTeacher);
		}
		if (idTest!=0) {
			getProperties.put("id_test", idTest);
		}
		if (idPupil!=0) {
			getProperties.put("id_pupil", idPupil);
		}
		if (progress!=-1) {
			getProperties.put("progress", progress);
		}
		return getProperties;
	}

	@Override
	public String getNameTable() {
	
		return "results";
	}

	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		Result variant = new Result();
		variant.setIdTeacher(resSet.getInt("id_teacher"));
		variant.setIdTest(resSet.getInt("id_test"));
		variant.setIdPupil(resSet.getInt("id_pupil"));
		variant.setProgress(resSet.getFloat("progress"));
		return variant;
	}

}
