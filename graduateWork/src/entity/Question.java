package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Question implements GetProperties {
	private int idQuestion =0;
	private int idTest=0;
	private String type=null;
	private int score=0;
	private String textQuestion=null;
	public Question(int idQuestion, int idTest, String type, int score, String textQuestion) {
		this.idQuestion = idQuestion;
		this.idTest = idTest;
		this.type = type;
		this.score = score;
		this.textQuestion = textQuestion;
	}
	public Question(){}
	public int getIdQuestion() {
		return idQuestion;
	}

	public void setIdQuestion(int idQuestion) {
		this.idQuestion = idQuestion;
	}

	public int getIdTest() {
		return idTest;
	}

	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public String getTextQuestion() {
		return textQuestion;
	}

	public void setTextQuestion(String textQuestion) {
		this.textQuestion = textQuestion;
	}

	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (idQuestion!=0) {
			getProperties.put("id_question", idQuestion);
		}
		if (idTest!=0) {
			getProperties.put("id_test", idTest);
		}
		if (type!=null) {
			getProperties.put("type", type);
		}
		if (score!=0) {
			getProperties.put("score", score);
		}
		if (textQuestion!=null) {
			getProperties.put("text_question", textQuestion);
		}
		
		
		return getProperties;
	}

	@Override
	public String toString() {
		return "Question [idQuestion=" + idQuestion + ", idTest=" + idTest + ", type=" + type + ", score=" + score
				+ ", textQuestion=" + textQuestion + "]";
	}
	@Override
	public String getNameTable() {
		
		return "questions";
	}
	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		Question question = new Question();
		question.setIdQuestion(resSet.getInt("id_question"));
		question.setIdTest(resSet.getInt("id_test"));
		question.setType(resSet.getString("type"));
		question.setScore(resSet.getInt("score"));
		question.setTextQuestion(resSet.getString("text_question"));
		return question;
	}

}
