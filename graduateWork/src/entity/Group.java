package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Group implements GetProperties {
	private int idGroup=0;
	private int idTeacher=0;
	private String titleGroup=null;
	public Group(int idGroup, int idTeacher, String titleGroup) {
		this.idGroup = idGroup;
		this.idTeacher = idTeacher;
		this.titleGroup = titleGroup;
	}
	public Group(){}

	public int getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(int idGroup) {
		this.idGroup = idGroup;
	}

	public int getIdTeacher() {
		return idTeacher;
	}

	public void setIdTeacher(int idTeacher) {
		this.idTeacher = idTeacher;
	}

	public String getTitleGroup() {
		return titleGroup;
	}

	public void setTitleGroup(String titleGroup) {
		this.titleGroup = titleGroup;
	}

	@Override
	public String toString() {
		return "Group [idGroup=" + idGroup + ", idTeacher=" + idTeacher + ", titleGroup=" + titleGroup + "]";
	}

	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (idGroup!=0) {
			getProperties.put("id_group", idGroup);
		}
		if (idTeacher!=0) {
			getProperties.put("id_teacher", idTeacher);
		}
		if (titleGroup!=null) {
			getProperties.put("title_group", titleGroup);
		}
		
		
		
		return getProperties;
	}

	@Override
	public String getNameTable() {
		return "groups";
	}

	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		Group group = new Group();
		group.setIdGroup(resSet.getInt("id_group"));
		group.setIdTeacher(resSet.getInt("id_teacher"));
		group.setTitleGroup(resSet.getString("title_group"));
		return group;
	}

}
