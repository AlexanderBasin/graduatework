package entity;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class Test implements  GetProperties {
	private int idTest=0;
	private int idTeacher = 0;
	private int maxTime=0;
	private String titleTest=null;
	
	public Test(int idTest, int idTeacher, int maxTime, String titleTest) {
		this.idTest = idTest;
		this.idTeacher = idTeacher;
		this.maxTime = maxTime;
		this.titleTest = titleTest;
	}
	public Test(){}

	public int getIdTest() {
		return idTest;
	}

	public void setIdTest(int idTest) {
		this.idTest = idTest;
	}

	public int getIdTeacher() {
		return idTeacher;
	}

	@Override
	public String toString() {
		return "Test [idTest=" + idTest + ", idTeacher=" + idTeacher + ", maxTime=" + maxTime + ", titleTest="
				+ titleTest + "]";
	}
	public void setIdTeacher(int idTeacher) {
		this.idTeacher = idTeacher;
	}

	public int getMaxTime() {
		return maxTime;
	}

	public void setMaxTime(int maxTime) {
		this.maxTime = maxTime;
	}

	public String getTitleTest() {
		return titleTest;
	}

	public void setTitleTest(String titleTest) {
		this.titleTest = titleTest;
	}

	@Override
	public HashMap<String, Object> getProperties() {
		HashMap<String, Object> getProperties =new HashMap<String, Object>();
		if (idTest!=0) {
			getProperties.put("id_test", idTest);
		}
		if (idTeacher!=0) {
			getProperties.put("id_teacher", idTeacher);
		}
		if (maxTime!=0) {
			getProperties.put("time", maxTime);
		}
		if (titleTest!=null){
			getProperties.put("title_test", titleTest);
		}
		return getProperties;
	}

	@Override
	public String getNameTable() {
		return "tests";
	}
	@Override
	public GetProperties setProperties(ResultSet resSet) throws SQLException {
		Test test = new Test();
		test.setIdTest(resSet.getInt("id_test"));
		test.setIdTeacher(resSet.getInt("id_teacher"));
		test.setMaxTime(resSet.getInt("time"));
		test.setTitleTest(resSet.getString("title_test"));
		return test;
	}
	
}
