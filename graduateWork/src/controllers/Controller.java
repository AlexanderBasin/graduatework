package controllers;

import java.sql.SQLException;
import java.util.ArrayList;

import db.QueryDB;
import entity.GetProperties;


public class Controller {
	public static void create (GetProperties getProperties) throws ClassNotFoundException, SQLException{
		QueryDB.insert(getProperties);
	}
	public static ArrayList find (GetProperties getProperties) throws ClassNotFoundException, SQLException{
		
		return QueryDB.select(getProperties);
		
	}
	public static void delete(GetProperties getProperties) throws ClassNotFoundException, SQLException {
		QueryDB.delete(getProperties);
		
	}
	public static void edit(GetProperties changed, GetProperties result) throws ClassNotFoundException, SQLException {
		QueryDB.update(changed,result);
	}
	public static int getMaxId (GetProperties getProperties) throws ClassNotFoundException, SQLException {
		return QueryDB.getMaxId(getProperties);
	}
}
