package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import entity.GetProperties;




public class QueryDB  {
	
	
	private static String url ="jdbc:sqlite::resource:http://localhost:8080/graduateWork/db/TestsBase.s3db";
	private static	  Connection conn;
	private static void connect() throws ClassNotFoundException, SQLException {
		Class.forName("org.sqlite.JDBC");
		conn=  DriverManager.getConnection (url);
		//System.out.println("База Подключена!");
	}
	
	private static void close() throws SQLException{
		 conn.close();
	}
	private static void sendOptions (HashMap<String, Object> properties,PreparedStatement stmt,int k) throws NumberFormatException, SQLException{
		   for ( Map.Entry<String, Object> entry:properties.entrySet()){ 
			   if (entry.getValue().getClass().getName().equals("java.lang.Integer")) {
				   stmt.setInt(k, Integer.parseInt(entry.getValue().toString()));
			   }
			   if (entry.getValue().getClass().getName().equals("java.lang.String")) {
				  stmt.setString(k, entry.getValue().toString());
			   }
			   if (entry.getValue().getClass().getName().equals("java.lang.Boolean")) {
				   stmt.setBoolean(k, Boolean.parseBoolean(entry.getValue().toString()));
			   }
			   if (entry.getValue().getClass().getName().equals("java.lang.Float")) {
				   stmt.setFloat(k, Float.parseFloat(entry.getValue().toString()));
			   }
			   
			  k++;
		   }
	}
	public static void insert (GetProperties getProperties) throws ClassNotFoundException, SQLException{
		connect();
		int countProperties = getProperties.getProperties().size();
		String sql ="INSERT INTO "+getProperties.getNameTable()+" (";
		int j = 1;
		for ( Map.Entry<String, Object> entry:getProperties.getProperties().entrySet()){
			if (j==countProperties) {
				sql+=entry.getKey();
			} else{
				sql+=entry.getKey()+",";
			}
			j++;
		}
		sql+=") VALUES(";
		
		int p =1;
		for (int i=0;i<countProperties;i++) {
			if (p==countProperties) {
				sql+="?";
			} else{
				sql+="?,";
			}
			p++;
		}
		sql+=")";
		   PreparedStatement stmt = conn.prepareStatement(sql);
		   sendOptions ( getProperties.getProperties(), stmt,1);
		   stmt.executeUpdate();
		   stmt.close();
		   close();
		
	}
	
	public static ArrayList <GetProperties> select (GetProperties getProperties) throws ClassNotFoundException, SQLException{
		ArrayList <GetProperties> select = new   ArrayList<GetProperties>() ;	
		if (getProperties.getProperties().size() ==0) return select;
		connect();
		
		String sql = "SELECT * FROM "+getProperties.getNameTable()+" WHERE ";
		int j=1;
		for ( Map.Entry<String, Object> entry:getProperties.getProperties().entrySet()){
			
			if (j==1) {
				sql+=entry.getKey()+" = ? ";
			} else {
				sql+=" AND "+entry.getKey()+" = ? ";
			}
			j++;
		}
		PreparedStatement stmt = conn.prepareStatement(sql);
		sendOptions ( getProperties.getProperties(), stmt,1);
		ResultSet resSet = stmt.executeQuery();
		
		while(resSet.next()) {
			select.add(getProperties.setProperties(resSet));
			
		}
		close();
		 stmt.close();
		
		return select;
	}
	
	public static int getMaxId (GetProperties getProperties) throws SQLException, ClassNotFoundException {
		connect();
		int k=0;
		String key=null;
		for ( Map.Entry<String, Object> entry:getProperties.getProperties().entrySet()){
			if (k==1) break;
			key=entry.getKey();
			k++;
		}
		String sql = "SELECT MAX("+key+") FROM "+getProperties.getNameTable();
		PreparedStatement stmt = conn.prepareStatement(sql);
		ResultSet resSet = stmt.executeQuery();
		int maxId =0;
		if (resSet.next()) {
			maxId = resSet.getInt("MAX("+key+")");
		}
		stmt.close();
		close();
		return maxId;
	}
	
	public static void delete (GetProperties getProperties) throws ClassNotFoundException, SQLException {	
		connect();
		String sql = "DELETE  FROM "+getProperties.getNameTable()+" WHERE ";
		int j=1;
		for ( Map.Entry<String, Object> entry:getProperties.getProperties().entrySet()){
			
			if (j==1) {
				sql+=entry.getKey()+" = ? ";
			} else {
				sql+=" AND "+entry.getKey()+" = ? ";
			}
			j++;
		}
		PreparedStatement stmt = conn.prepareStatement(sql);
		sendOptions ( getProperties.getProperties(), stmt,1);
		stmt.executeUpdate();
		stmt.close();
		close();
		
		
	}
	
	public static void update (GetProperties changed,GetProperties result) throws ClassNotFoundException, SQLException {
		connect();
		int countProperties = result.getProperties().size();
		String sql ="UPDATE "+result.getNameTable()+" SET ";
		int p = 1;
		for ( Map.Entry<String, Object> entry:result.getProperties().entrySet()){
			if (p==countProperties) {
				sql+=entry.getKey()+" =? ";
			} else{
				sql+=entry.getKey()+" =?, ";
			}
			p++;
		}
		sql+= " WHERE ";
		int j=1;
		for ( Map.Entry<String, Object> entry:changed.getProperties().entrySet()){
			
			if (j==1) {
				sql+=entry.getKey()+" = ? ";
			} else {
				sql+=" AND "+entry.getKey()+" = ? ";
			}
			j++;
		}
		PreparedStatement stmt = conn.prepareStatement(sql);
		
		sendOptions ( result.getProperties(), stmt,1);
		sendOptions ( changed.getProperties(), stmt,countProperties+1);
		   
		   
		   stmt.executeUpdate();
		   stmt.close(); 
		   close();
		
		
	}
}
