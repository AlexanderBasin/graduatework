package operation;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controllers.Controller;
import entity.Group;
import entity.Pupil;
import entity.PupilInGroup;
import entity.Teacher;
import entity.Test;
import entity.TestInGroup;

/**
 * Servlet implementation class OperationWithGroupServlet
 */
@WebServlet("/OperationWithGroups")
public class OperationWithGroupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Teacher currentUser =   (Teacher) (session.getAttribute("currentSessionUser"));
		request.setCharacterEncoding("UTF-8");
		int id_teacher = currentUser.getIdTeacher();
		try {
			if (request.getParameter("add_test_in_group")!=null) { 
				int idGroup = Integer.parseInt( request.getParameter("id_group"));
				String titleTest = request.getParameter("title_test_group");
				Test test = new Test(0,id_teacher,0,titleTest);
				test = (Test) Controller.find(test).get(0);
				int idTest = test.getIdTest();
				Controller.create(new TestInGroup(idGroup,idTest));
			}
			if (request.getParameter("add_pupil_in_group")!=null) { 
				int idGroup = Integer.parseInt( request.getParameter("id_group"));
				String loginPupil = request.getParameter("login_pupil");
				Pupil pupil = new Pupil(0,loginPupil,null);
				pupil = (Pupil)Controller.find(pupil).get(0);
				int idPupil = pupil.getIdPupil();
				Controller.create(new PupilInGroup(idGroup,idPupil));
			}
			if (request.getParameter("add_group")!=null){
				String titleGroup = request.getParameter("title_group");
				Controller.create(new Group(0,id_teacher,titleGroup));
			}
			
			if (request.getParameter("change_group")!=null){
				String titleGroup = request.getParameter("new_title_group");
				int idGroup = Integer.parseInt(request.getParameter("id_group"));
				Controller.edit(new Group(idGroup,0,null),new Group(0,0,titleGroup));
			
			}
			if ( request.getParameter("del_test_in_group")!=null){
				String[] parts = request.getParameter("del_test_in_group").split("\\|");
				int idTest = Integer.parseInt(parts[0] );
				int idGroup = Integer.parseInt(parts[1] );
				Controller.delete(new TestInGroup(idGroup,idTest));
				
			}
			
			if ( request.getParameter("del_pupil_in_group")!=null){
				String[] parts = request.getParameter("del_pupil_in_group").split("\\|");
				int idPupil = Integer.parseInt( parts[0]);
				int idGroup = Integer.parseInt(parts[1] );
				Controller.delete(new PupilInGroup(idGroup,idPupil));
				
			}
			
			if ( request.getParameter("del_group")!=null){
				int idGroup = Integer.parseInt( request.getParameter("del_group"));
				Controller.delete(new Group(idGroup,0,null));
				Controller.delete(new PupilInGroup(idGroup,0));
				Controller.delete(new TestInGroup(idGroup,0));
			}
			
		}

		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect("teacherProfile.jsp");
	}

}
