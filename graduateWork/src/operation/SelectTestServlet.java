package operation;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controllers.Controller;
import entity.Pupil;
import entity.Question;
import entity.Result;
import entity.Test;
import entity.Variant;

/**
 * Servlet implementation class SelectTestServlet
 */
@WebServlet("/SelectTest")
public class SelectTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		request.setCharacterEncoding("UTF-8");
		try {
		int idTest = Integer.parseInt( request.getParameter("idTest"));
		
			Test test =(Test)Controller.find(new Test(idTest,0,0,null)).get(0);
			ArrayList <Question> questions = (ArrayList<Question>)Controller.find(new Question(0,idTest,null,0,null));
			float sumScore=0;
			for (Question question:questions) {
				sumScore+=question.getScore();
			}
			HttpSession ses =request.getSession();
		ses.setAttribute("sumScore", sumScore);
		ses.setAttribute("numberQuestion", 0);
		ses.setAttribute("test", test);
		int [] results = new int[questions.size()];
		ses.setAttribute("results",results);
		HashMap<String, Object> [] historyAnswers = new HashMap[questions.size()];
		
		ses.setAttribute("historyAnswers", historyAnswers);
		RequestDispatcher Dispatcher = getServletContext().getRequestDispatcher("/pupilProfile.jsp");
        Dispatcher.forward(request, response);}
		catch(Throwable theException){
			 System.out.println(theException); 
		}
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException { 
		request.setCharacterEncoding("UTF-8");
		HttpSession ses =request.getSession();
		int numberQuestion = 0;
		
		if ( request.getParameter("exit")!=null ) {
			ses.invalidate();
			response.sendRedirect("index.jsp");
			
		}
		if ( request.getParameter("next")!=null ) {
			int current = Integer.parseInt(request.getParameter("next"));
			ses.setAttribute("numberQuestion", current+1);
			numberQuestion=current;
		}
		
		if ( request.getParameter("prev")!=null ) {
			int current = Integer.parseInt(request.getParameter("prev"));
			ses.setAttribute("numberQuestion", current-1);
			numberQuestion=current;
		}
		
		
		if ( request.getParameter("send")!=null ) {
			int current = Integer.parseInt(request.getParameter("send"));
			numberQuestion=current;
		}
		if ( request.getParameter("exit")==null ) {
	
			
			int [] results = (int[]) ses.getAttribute("results"); 
			HashMap<String, Object> [] historyAnswers =  (HashMap<String, Object>[]) ses.getAttribute("historyAnswers");
			int score = (int) ses.getAttribute("score");
			ArrayList <Variant> variants = (ArrayList<Variant>) ses.getAttribute("variants");
			int countVariants = variants.size();
			
			if (request.getParameter("answerRadio")!=null) {
			 int selectVariant = Integer.parseInt(request.getParameter("answerRadio"));
			 historyAnswers[numberQuestion] = new HashMap<String, Object>();
			 historyAnswers[numberQuestion].put("radio", selectVariant);
			 if (variants.get(selectVariant).isAnswer()) { 
				 results[numberQuestion]=score; 
				 } else{ 
					 results[numberQuestion]=0; 
				 } 
			} else if (request.getParameter("answerText")!=null && request.getParameter("answerText")!=""){
				historyAnswers[numberQuestion] = new HashMap<String, Object>();
				historyAnswers[numberQuestion].put("text", request.getParameter("answerText"));
				if( variants.get(0).getTextVariant().equals( request.getParameter("answerText"))){ 
					results[numberQuestion]=score; 
				}
				else {
					results[numberQuestion]=0;
				}
			} else {
				int countSelectVariants = 0;
				int countSelectRightVariants = 0;
				int countRightVariants=0;
				int [] answers = new int[countVariants];
				for (int i=0;i<countVariants;i++){
					answers[i] =-1;
					if (variants.get(i).isAnswer()) countRightVariants++;
					if (request.getParameter("answerCheck"+i)!=null) {
						answers[i]=i;
						countSelectVariants++;
						if(variants.get(i).isAnswer()){
							countSelectRightVariants++;
						}
					}
					
				}
				
				if(countSelectVariants!=0) {
					
					historyAnswers[numberQuestion] = new HashMap<String, Object>();
					historyAnswers[numberQuestion].put("check", answers);
					if(countSelectVariants==countSelectRightVariants && countSelectRightVariants==countRightVariants) {
						results[numberQuestion]=score;
					}
					
					else if (  (countSelectVariants - countSelectRightVariants ==1  && countSelectRightVariants== countRightVariants)
							|| (  countRightVariants - countSelectVariants ==1 && countSelectVariants== countSelectRightVariants)){
						results[numberQuestion]=(int) (score*0.2);
					} else{
						results[numberQuestion] = 0;
					}
				}	
				
			}
			
			 for (int i=0;i<results.length;i++) { 
				 System.out.println(" results [ index = "+i+" element = "+results[i]+" ] "); 
			} 
			 
			 for (int i=0;i<historyAnswers.length;i++) { 
				 System.out.println(" history [ index = "+i+" element = "+historyAnswers[i]+" ] "); 
			} 
			ses.setAttribute("results",results);
			ses.setAttribute("historyAnswers", historyAnswers);
			RequestDispatcher Dispatcher = getServletContext().getRequestDispatcher("/pupilProfile.jsp");
	        Dispatcher.forward(request, response);
			
		}
		
		
		if ( request.getParameter("send")!=null ) {
			try {
				Test test =    (Test)ses.getAttribute("test");
				Pupil currentUser =   (Pupil) (ses.getAttribute("currentSessionUser"));
				int [] results = (int[]) ses.getAttribute("results");
				float sumScore = (float) ses.getAttribute("sumScore");
				double sumResult=0;
				
				for(int result:results) {
					sumResult+=result;
				}
				float progress = (float) ((sumResult/sumScore)*100);
	
				//System.out.println(progress);
				Controller.delete(new Result(test.getIdTeacher(),test.getIdTest(),currentUser.getIdPupil(),-1));
				Controller.create(new Result(test.getIdTeacher(),test.getIdTest(),currentUser.getIdPupil(),progress));
				
			} catch (ClassNotFoundException | SQLException e) {
				e.printStackTrace();
			}
		}
	}

	
}
