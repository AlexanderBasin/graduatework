package operation;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controllers.Controller;
import entity.PupilInGroup;
import entity.Result;
import entity.Teacher;


@WebServlet("/OperationWithResults")
public class OperationWithResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Teacher currentUser =   (Teacher) (session.getAttribute("currentSessionUser"));
		request.setCharacterEncoding("UTF-8");
		int id_teacher = currentUser.getIdTeacher();
		try {
			if ( request.getParameter("del_result_pupil")!=null){
				String[] parts = request.getParameter("del_result_pupil").split("\\|");
				int idTest = Integer.parseInt( parts[0]);
				int idPupil = Integer.parseInt(parts[1] );
				Controller.delete(new Result(id_teacher,idTest,idPupil,-1));
				
			}
			if ( request.getParameter("del_result")!=null){
				
				int idTest = Integer.parseInt( request.getParameter("del_result"));
				Controller.delete(new Result(id_teacher,idTest,0,-1));
				
			}
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		response.sendRedirect("teacherProfile.jsp");
	}

}
