package operation;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import controllers.Controller;
import entity.Group;
import entity.Pupil;
import entity.PupilInGroup;
import entity.Question;
import entity.Teacher;
import entity.Test;
import entity.TestInGroup;
import entity.Variant;

/**
 * Servlet implementation class OperationWithTestServlet
 */ 
@WebServlet("/OperationWithTests")
public class OperationWithTestServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			HttpSession session = request.getSession();
			Teacher currentUser =   (Teacher) (session.getAttribute("currentSessionUser"));
			request.setCharacterEncoding("UTF-8");
			int id_teacher = currentUser.getIdTeacher();
			try {
			
		
				if (request.getParameter("exit")!=null){
					session.invalidate();
					 response.sendRedirect("index.jsp");
					 return;
					
				}
				
					
				if (request.getParameter("add_test")!=null){
					String titleTest = request.getParameter("title_test");
					int time = Integer.parseInt(request.getParameter("time"));
					Controller.create(new Test(0,id_teacher,time,titleTest));
				
				}
				
				
				if (request.getParameter("change_test")!=null){
					String titleTest = request.getParameter("new_title_test");
					int time = Integer.parseInt(request.getParameter("new_time"));
					int idTest = Integer.parseInt(request.getParameter("id_test"));
					Controller.edit(new Test(idTest,0,0,null),new Test(0,0,time,titleTest));
				
				}
				
				
				
				if (request.getParameter("add_question")!=null){
					String textQuestion = request.getParameter("text_question");
					int score = Integer.parseInt( request.getParameter("score"));
					String type = request.getParameter("type");
					int idTest = Integer.parseInt(request.getParameter("id_test"));
					Controller.create(new Question(0,idTest,type,score,textQuestion));
					int maxId = Controller.getMaxId(new Question(3,0,null,0,null));
					if (!type.equals("n")){
						int countVariants = Integer.parseInt(request.getParameter("count_variants"));
						for (int i=1;i<=countVariants;i++) {
							boolean answer =false;
							if (request.getParameter("answer"+i).equals("yes")) answer = true;
							Controller.create(new Variant(0,maxId,request.getParameter("text_variant"+i),answer));
						}
					}
					
					if (type.equals("n")){
						Controller.create(new Variant(0,maxId,request.getParameter("text_variant"),true));
					}
	
				}
				
				if ( request.getParameter("del_question")!=null){
					int idQuestion = Integer.parseInt( request.getParameter("del_question"));
					Controller.delete(new Question(idQuestion,0,null,0,null));
					Controller.delete(new Variant(0,idQuestion,null,false));
				}
				
				
				
				if ( request.getParameter("del_test")!=null){
					
					int idTest = Integer.parseInt( request.getParameter("del_test"));
					
				for ( Question question: (ArrayList<Question>) Controller.find(new Question(0,idTest,null,0,null)) ) {
					Controller.delete(new Variant(0,question.getIdQuestion(),null,false));
				}
					
					Controller.delete(new Test(idTest,0,0,null));
					Controller.delete(new Question(0,idTest,null,0,null));
					
				
				}
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			response.sendRedirect("teacherProfile.jsp");
		
	}

}
