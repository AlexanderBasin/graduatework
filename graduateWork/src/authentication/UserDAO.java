package authentication;


import controllers.Controller;
import entity.Teacher;
import entity.User;

import java.sql.*;

public class UserDAO 	
{
   static Connection currentCon = null;
   static ResultSet rs = null;  
	
	
	
   public static User login(User bean) throws ClassNotFoundException, SQLException {
	
      
	    
   try 
   {
      
	   boolean more =  !(Controller.find(bean).isEmpty());
	 
	       
      // if user does not exist set the isValid variable to false
      if (!more) 
      {
         System.out.println("Sorry, you are not a registered user! Please sign up first");
         bean.setValid(false);
      } 
	        
      //if user exists set the isValid variable to true
      else if (more) 
      {
        bean = (User) Controller.find(bean).get(0);
         bean.setValid(true);
      }
   } 

   catch (Exception ex) 
   {
      System.out.println("Log In failed: An Exception has occurred! " + ex);
   } 
	    
   //some exception handling
 

return bean;
	
   }	
}

