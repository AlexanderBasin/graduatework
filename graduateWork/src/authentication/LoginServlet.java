package authentication;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import db.QueryDB;
import entity.Pupil;
import entity.Teacher;
import entity.User;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/login/LoginServlet")
public class LoginServlet extends HttpServlet {


/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

public void doGet(HttpServletRequest request, HttpServletResponse response) 
			           throws ServletException, java.io.IOException {

try
{	    

	User user = new Pupil();
	String role = request.getParameter("role");
     if (role.equals("teacher")) {
    	 user = new Teacher();
     }
     
     user.setLogin(request.getParameter("un"));
     user.setPassword(request.getParameter("pw")); 

     user = UserDAO.login(user);
	   		    
     if (user.isValid())
     {
	        
          HttpSession session = request.getSession();
         
         
         
          
          if (role.equals("teacher")) {
        	  user.setNameRole("teacher");
        	  session.setAttribute("currentSessionUser",user);
        	  response.sendRedirect("../teacherProfile.jsp"); //logged-in page 
          }  
          if (role.equals("pupil")) {
        	  user.setNameRole("pupil");
        	  session.setAttribute("currentSessionUser",user);
        	  session.setAttribute("test",null);
        	  response.sendRedirect("../pupilProfile.jsp"); //logged-in page 
        	  
          } 
     }
	        
     else 
          response.sendRedirect("invalidLogin.jsp"); //error page 
} 
		
		
catch (Throwable theException) 	    
{
     System.out.println(theException); 
}
       }
	}
