package authentication;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controllers.Controller;
import entity.Pupil;
import entity.Teacher;
import entity.User;

/**
 * Servlet implementation class RegestrationServlet
 */
@WebServlet("/login/RegestrationServlet")
public class RegestrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
      
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try
		{	    
			User user = new Pupil();
			String role = request.getParameter("role");
		     if (role.equals("teacher")) {
		    	 user = new Teacher();
		     }
		     user.setLogin(request.getParameter("login"));
		     user = UserDAO.login(user);
			   		    
		     if (user.isValid())
		     {
		    	 response.sendRedirect("invalidLogin.jsp"); //error page   
		    	 
		               		
		     }
			        
		     else {
		    	 user.setPassword(request.getParameter("password")); 
		    	 Controller.create(user);
		    	 response.sendRedirect("../index.jsp");
		     }
		} 
				
				
		catch (Throwable theException) 	    
		{
		     System.out.println(theException); 
		}
	}

}
