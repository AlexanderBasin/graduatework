<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="entity.Teacher,entity.Group,controllers.Controller,entity.Test,entity.Question,entity.Variant,
    java.util.ArrayList,entity.GetProperties,entity.TestInGroup,entity.PupilInGroup,entity.Pupil,java.util.HashMap"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%@ include file="head.jsp" %>
            <% Pupil currentUser =   (Pupil) (session.getAttribute("currentSessionUser"));%>
	<div id="content">
		<ul id="groups">
			<%
				for (PupilInGroup pupilInGroup : (ArrayList<PupilInGroup>)Controller.find(new PupilInGroup(0,currentUser.getIdPupil())) ) {
					int idGroup = pupilInGroup.getIdGroup();
					Group group = (Group) Controller.find(new Group(idGroup,0,null)).get(0);
			%>
			<li class="group">
				<label>
					<span><b><%=group.getTitleGroup() %></b></span>
				</label>
				<ul id ="tests">
					<%
						for(TestInGroup testInGroup:(ArrayList<TestInGroup>)Controller.find(new TestInGroup(idGroup,0))) {
							int idTest = testInGroup.getIdTest();
							Test test = (Test)Controller.find(new Test(idTest,0,0,null)).get(0);
					%>
					<li class="test">
						<label onclick="window.location.href = '${pageContext.request.contextPath}/SelectTest?idTest=<%=idTest %>'">
							<span><%=test.getTitleTest() %></span>
							
						</label>
					</li>
					
					
					<%} %>
				</ul>
			</li>
			
			
			
			<%} %>
		</ul>
		<form  method="post" action="SelectTest">
			<button name="exit" value="exit">exit</button>
			<div id="start_test">
				<% 
					if (request.getSession().getAttribute("test")!=null) {
						HttpSession ses =request.getSession();
						Test test =    (Test)ses.getAttribute("test");
						ArrayList <Question> questions = Controller.find(new Question(0,test.getIdTest(),null,0,null));
						int numberQuestion =  (Integer)  ses.getAttribute("numberQuestion");
						Question question = questions.get(numberQuestion);
						ArrayList <Variant> variants = Controller.find(new Variant(0,question.getIdQuestion(),null,false));
						ses.setAttribute("variants", variants);
						ses.setAttribute("score", question.getScore());
						HashMap<String, Object> [] historyAnswers =  (HashMap<String, Object>[]) ses.getAttribute("historyAnswers");
				%>
				<span><%=test.getTitleTest() %></span>
				<div id="question">
					<%
						String type="";
						if(question.getType().equals("r")) type="radio";
						if(question.getType().equals("c")) type="checkbox";
						if(question.getType().equals("n")) type="text";
					%>
					<span><%=question.getTextQuestion() %></span>
					<%for (int i=0;i<variants.size();i++)  {%>
						
					<div class="variant">
						<%if(type.equals("radio")) {%>
							<%
								String checked = "";
								if (historyAnswers[numberQuestion]!=null && historyAnswers[numberQuestion].get("radio").equals(i) )
									checked = "checked";
								
							%>
							<input type='<%=type%>' name="answerRadio" value='<%=i %>' <%=checked %>>
						<%}  if(type.equals("text")){ %>
							<%
								String value="";
								if (historyAnswers[numberQuestion]!=null) 
									value =String.valueOf(historyAnswers[numberQuestion].get("text"));
							%>
							<input type='<%=type%>' name="answerText" value="<%=value%>">
						<%} if(type.equals("checkbox")){ %>
							<%
								String checked = ""; 
								if (historyAnswers[numberQuestion]!=null) {
									int [] selected = (int []) historyAnswers[numberQuestion].get("check");
									if ( selected[i]!=-1 )
										checked = "checked";
								}
							%>
							<input type='<%=type%>' name="answerCheck<%=i %>" <%=checked %> >
						<%} %>	
						<%if(!type.equals("text")) {%>
							<span><%=variants.get(i).getTextVariant() %></span>
						<%} %>
					</div>
					<%} %>
				</div>
				<br>
				<%if (numberQuestion!=0) {%>
					<button name="prev" value="<%=numberQuestion%>">prev</button>
				<%} %>
				<% if (numberQuestion!=questions.size()-1) { %>
					<button name="next" value="<%=numberQuestion%>">next</button>
				<%} %>
				<button name="send" value="<%=numberQuestion%>">send</button>
				<%} %>
			</div>
		</form>
	</div>
	
	
           
        
</body>
</html>