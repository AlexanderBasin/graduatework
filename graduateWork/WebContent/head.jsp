<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="entity.User,entity.Teacher"%>
 <%
 	HttpSession sess = request.getSession(true);
 	 
 	if (sess.isNew()) {
 		Cookie cookie = new Cookie("JSESSIONID", sess.getId());
 		cookie.setMaxAge(31536000);
 		response.reset();
 		response.setCharacterEncoding("UTF-8");
 		response.addCookie(cookie);
 	}
 	
         
 %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-3.1.1.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/check.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/style.css">
</head>
<body>
	<div id="content">
	<header>
		<ul>
			<li><a href="${pageContext.request.contextPath}/index.jsp">Главная</a></li>
			<li><a href="${pageContext.request.contextPath}/login/LoginPage.jsp">Вход</a></li>
			<li><a href="${pageContext.request.contextPath}/login/regestration.jsp">Регестрация</a></li>
		</ul>
		<%
			if(session.getAttribute("currentSessionUser")!=null) {
			 User user  = (User) (session.getAttribute("currentSessionUser"));
			 if(user.getNameRole().equals("teacher")) { 
		 %>
			<span>Перейти к</span>	 <a href='${pageContext.request.contextPath}/teacherProfile.jsp'> <%= user.getLogin()%></a>
		<% } if(user.getNameRole().equals("pupil")) {%>
			<span>Перейти к</span> <a href='${pageContext.request.contextPath}/pupilProfile.jsp'> <%= user.getLogin()%></a>
		<%}} %>
	</header>
	</div>	
</body>
</html>