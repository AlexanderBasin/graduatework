<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="entity.Teacher,entity.Group,controllers.Controller,entity.Test,entity.Question,entity.Variant,
    java.util.ArrayList,entity.GetProperties,entity.TestInGroup,entity.PupilInGroup,entity.Pupil,entity.Result" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/operationWithTests.js"></script>
</head>
<body>
	<%@ include file="head.jsp" %>
	<center>
            <% Teacher currentUser =   (Teacher) (session.getAttribute("currentSessionUser"));%>
			
           
         </center>
	<div id="content">
	<div id="id"></div>
	
  <form action="OperationWithTests" method="post">
  	<ul style="display:inline-block">
	  	<li><button name="exit" value="exit">Выход  </button></li>
	  	<li><button   onclick="addTest()" type="button">Добавить тест  </button></li>
  	</ul><br>
  	
  	<ul id = "tests">
  		<li>
	  		<table id="add_test" class="hidden">
	  	   <tr>
	  	   		<td><span>Название</span></td>
	  	   		<td><input type="text" name="title_test"></td> 
	  	   </tr>
	  	   <tr>
	  	   		<td><span>Время выполнения</span></td>
	  	   		<td><input type="text" name="time"></td> 
	  	   </tr>
	  	   <tr> <td><button name="add_test" value="add_test">Добавить  </button> </td></tr>
	  		</table>
  		</li>
  		<li><h3>Мои тесты</h3></li>
  		<% 
  		if (session.getAttribute("currentSessionUser")!=null) {
  			GetProperties t ;
  			for( Test test :(ArrayList<Test>) Controller.find(new Test(0,currentUser.getIdTeacher(),0,null)) )  {
  			
  		%>
  		<li class = "test"  >
  			<label title="время выполнения <%=test.getMaxTime() %>" onclick="showQuestions(this)"><span><%=test.getTitleTest() %> </span></label>
  			<button type="button"  class="btn_change_test" onclick="changeTest(this,<%=test.getIdTest()%>,<%=test.getMaxTime()%>,'<%=test.getTitleTest()%>')">~</button> 
 			<button type="button" onclick="addQuestion(this,<%=test.getIdTest()%>)">+</button> 
 			<button name="del_test" value=<%=test.getIdTest() %> >-</button> 
  			<ul class="questions hidden">
  				<%
  					for (Question question:  (ArrayList<Question>)Controller.find( new Question (0,test.getIdTest(),null,0,null))  ) {
  				%>
  				<li class="question">
  					<%
  						String type ="тип неизвестен";
  						if (question.getType().equals("c")) type = "множественный выбор";
  						if (question.getType().equals("r")) type = "одиночный выбор";
  						if (question.getType().equals("n")) type = "без вариантов";
  					%>
  					<label onclick="showQuestions(this)" style="color:green" title="<%=question.getScore() %> очков, <%=type %>"     ><span><%=question.getTextQuestion() %></span></label>
  					<button style="color:green" name="del_question" value=<%=question.getIdQuestion() %>>x</button> 
  					<ul class="variants hidden">
  						<%for (Variant variant:  (ArrayList<Variant>)Controller.find(new Variant(0,question.getIdQuestion(),null,false))    ) {%>
  							<li class="variant"> 
  								<span ><%=variant.getTextVariant() %></span>
  								<%if (variant.isAnswer()) {%>
  									<input style="" disabled type ="checkbox" class="as" checked>
  								<% }else{ %>
  									<input disabled type ="checkbox" class="as">
  								<%} %>
  							</li>
  						<%} %>
  					</ul>
  				</li>
  			<% } %>	
  			</ul>
  		</li>
  		
  		<%}  %>
  	</ul>
  	</form>
  	<form action="OperationWithGroups" method="post">
  	<ul id="groups">
  		<li><h3>Мои группы</h3></li>
  		<li><button   onclick="addGroup()" type="button">Добавить группу  </button></li>
  		<li>
	  		<table id="add_group" class="hidden">
		  	   <tr>
		  	   		<td><span>Название</span></td>
		  	   		<td><input type="text" name="title_group"></td> 
		  	   </tr>
		  	   <tr> <td><button name="add_group" value="add_group">Добавить  </button> </td></tr>
		  	</table>
		 </li> 	
		  	<%
		  		for( Group group :(ArrayList<Group>) Controller.find(new Group(0,currentUser.getIdTeacher(),null)) ) { 
		  	%>
		  	<li class="group">
		  		<label onclick="showGroups(this)"><span><%=group.getTitleGroup() %> </span></label>
		  		<button name="del_group" value=<%=group.getIdGroup() %> >-</button>
		  		<button type="button"   onclick="changeGroup(this,<%=group.getIdGroup()%>,'<%=group.getTitleGroup()%>')">~</button>  
		  		<table class="hidden">
		  			<tr>
		  				<th colspan="2">тесты </th>
		  				<th colspan="2"> ученики</th>
		  			</tr>
		  			<%   //for(TestInGroup testInGroup: (ArrayList<TestInGroup>) Controller.find(new TestInGroup(group.getIdGroup(),0)) )
		  				ArrayList<TestInGroup> testInGroups = (ArrayList<TestInGroup>) Controller.find(new TestInGroup(group.getIdGroup(),0));
		  				ArrayList<PupilInGroup> pupilInGroups = (ArrayList<PupilInGroup>) Controller.find(new PupilInGroup(group.getIdGroup(),0));
		  				 for( int i=0;i< testInGroups.size() || i< pupilInGroups.size();i++){
		  					String titleTest="пусто";
		  					int idTest =0;
		  					if (testInGroups.size()>i) {
		  						idTest = testInGroups.get(i).getIdTest();
			  					Test test =new Test(idTest,0,0,null);
			  					test = (Test)Controller.find(test).get(0);
			  					titleTest = test.getTitleTest();
		  					}
		  					
		  					String loginPupil="пусто";
		  					int idPupil=0;
		  					if (pupilInGroups.size()>i) {
		  						idPupil = pupilInGroups.get(i).getIdPupil();
			  					Pupil pupil =new Pupil(idPupil,null,null);
			  					pupil = (Pupil)Controller.find(pupil).get(0);
			  					loginPupil = pupil.getLogin(); 
		  					}
		  			%>
		  			<tr>
		  				<td><%=titleTest %></td><td> <button style="color:green" name="del_test_in_group" value=<%=idTest+"|"+group.getIdGroup() %>>x</button>   </td>
		  				<td><%=loginPupil %></td> <td><button style="color:green" name="del_pupil_in_group" value=<%=idPupil+"|"+group.getIdGroup() %>>x</button>  </td>
		  			</tr>
		  			<%} %>
		  			<tr>
		  				<td colspan="2"><button type="button" onclick="addTestInGroup(this,<%=group.getIdGroup()%>)">+</button> </td>
		  				<td colspan="2"> <button type="button" onclick="addPupilInGroup(this,<%=group.getIdGroup()%>)">+</button></td>
		  			</tr>
		  			
		  		</table>
		  	</li>
	  <%} %>	
  	</ul>
  </form>
  
  <form action="OperationWithResults" method="post">
  	<ul id="results">
  		<li><h3>Мои Результаты</h3></li>
  		<%
  			ArrayList<Result> results = (ArrayList<Result>) Controller.find(new Result(currentUser.getIdTeacher(),0,0,-1));
  			
  		outer:for(int i=0;i<results.size();i++) {
  				for(int k=0;k<i;k++) {
  					if(results.get(k).getIdTest()==results.get(i).getIdTest()) continue outer;
  				}
  				int idTest = results.get(i).getIdTest();
  				Test test = (Test)Controller.find(new Test(idTest,0,0,null)).get(0);
  		%>
  		<li class="result">
  			<label onclick="showResult(this)">
  				<span><%=test.getTitleTest() %></span>
  			</label>
  			<button name="del_result" value="<%=idTest%>">-</button>
  			<table class="hidden" id="pupils" >
  				<%
  					for(Result result: (ArrayList<Result>) Controller.find(new Result(currentUser.getIdTeacher(),idTest,0,-1))) {
  						int idPupil = result.getIdPupil();
  						Pupil pupil = (Pupil) Controller.find(new Pupil(idPupil,null,null)).get(0);
  				%>
	  				<tr class="pupil">
		  					<td>
		  						<%=pupil.getLogin() %>
		  					</td>
		  					<td>
		  						<%=String.format("%.2f", result.getProgress())  %>
		  					</td>
		  					<td> <button name="del_result_pupil" value="<%=idTest+"|"+idPupil%>">x</button> </td>
	  				</tr>
  				<% }%>
  			</table>
  		</li>
  		
  		
  		
  		<%} %>
  	</ul>
  </form>
  
  <%} %>
 </div>      
</body>
</html>